============
Installation
============

At the command line::

    $ pip install networking-avaya

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv networking-avaya
    $ pip install networking-avaya
