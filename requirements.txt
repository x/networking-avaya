# The order of packages is significant, because pip processes them in the order
# of appearance. Changing the order has an impact on the overall integration
# process, which may cause wedges in the gate later.

pbr>=1.6
eventlet!=0.18.3,>=0.18.2
neutron-lib>=0.4.0
SQLAlchemy<1.1.0,>=1.0.10
alembic>=0.8.4
six>=1.9.0
oslo.concurrency>=3.8.0
oslo.config>=3.14.0
oslo.db!=4.13.1,!=4.13.2,>=4.10.0
oslo.log>=1.14.0
oslo.messaging>=5.2.0
oslo.service>=1.10.0
oslo.utils>=3.16.0
requests>=2.10.0
