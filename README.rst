===============================
networking-avaya
===============================

The Avaya OpenStack Neutron plugins provide the Neutron ML2 type driver and mechanism driver to provision the network segment IDs against the ToR switches automatically.

* Free software: Apache license
* Documentation: http://docs.openstack.org/developer/networking-avaya
* Source: http://git.openstack.org/cgit/openstack/networking-avaya
* Bugs: http://bugs.launchpad.net/networking-avaya

Features
--------

* TODO
