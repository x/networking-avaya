# Copyright (c) 2016 Avaya, Inc
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

"""Initial DB version

Revision ID: f5241c762eb6
Revises: None
Create Date: 2015-04-16 00:00:00.000000

"""

# revision identifiers, used by Alembic.
revision = 'f5241c762eb6'
down_revision = None


def upgrade():
    """A no-op migration for marking the inital DB version."""
    pass
